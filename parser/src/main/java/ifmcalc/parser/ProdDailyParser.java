package ifmcalc.parser;

import ifmcalc.calculator.accumulation.input.ProdDailyEntry;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProdDailyParser {

    Logger logger;

    {
        logger = Logger.getLogger(getClass().getName());
    }

    public List<ProdDailyEntry> parseProdDailyCSV(String csv) throws IOException, ParseException {
        CSVFormat csvFormat = CSVFormat.EXCEL.withDelimiter('|').withHeader("Agency Code", "Agent Code", "Policy No",
                "Appropriation Date", "Single FYP", "Single Top-Up", "Regular FYP", "Regular Top-Up",
                "Regular Single Top-Up", "APE").withIgnoreHeaderCase().withIgnoreEmptyLines(true);

        CSVParser parser = CSVParser.parse(csv, csvFormat);
        List<ProdDailyEntry> res = new ArrayList<>();
        int row = 0;
        Date date = null;
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        for (CSVRecord record : parser) {
            // 1st row is of the type 31/08/2021|19|
            if (row == 0) {
                String dateString = record.get(0);
                date = format.parse(dateString);
                System.out.println(date);
            } else if (row == 1) {
                // do nothing
            } else {
                ProdDailyEntry prodDailyEntry = new ProdDailyEntry(
                        record.get("Agency Code"),
                        record.get("Agent Code"),
                        record.get("Policy No"),
                        format.parse(record.get("Appropriation Date")),
                        date.getMonth() + 1, date.getYear() + 1900,
                        new BigDecimal(record.get("Regular FYP")),
                        new BigDecimal(record.get("APE")));
                logger.log(Level.INFO, prodDailyEntry.toString());
                res.add(prodDailyEntry);
            }
            row++;
        }
        return res;
    }
}
