package ifmcalc.parser;

import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ifmcalc.calculator.tree_building.input.AgentEntry;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class AgentJSONParser {
    Logger logger;

    {
        logger = Logger.getLogger(getClass().getName());
    }

    /**
     * @param json A JSON string.
     * @return A list of AgentEntry.
     * @throws IOException
     */
    public List<AgentEntry> parseAgentJSON(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(
                JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS.mappedFeature(),
                true
        );

        List<AgentEntry> agentEntries = mapper.readValue(json, new TypeReference<List<AgentEntry>>() {
        });

        return agentEntries;
    }

}
