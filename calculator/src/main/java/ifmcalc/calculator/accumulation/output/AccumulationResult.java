package ifmcalc.calculator.accumulation.output;

import org.apache.commons.lang3.tuple.Pair;
import ifmcalc.calculator.accumulation.input.ProdDailyEntry;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Production only applies to Advisors and not iBuddies.
 */
public class AccumulationResult {

    public Integer getYear() {
        return year;
    }

    /**
     * Map of agent code to a list of (untouched row in proddaily raw file, and the accumulated APE up til then)
     * sorted by (ProdDailyEntry.Month, APE), sum up til then
     */

    private final Integer year;
    private final Map<String, List<Pair<ProdDailyEntry, BigDecimal>>> innerMap;


    public AccumulationResult(Integer year, Map<String, List<Pair<ProdDailyEntry, BigDecimal>>> innerMap) {
        this.year = year;
        this.innerMap = innerMap;
    }

    /**
     * @return List of agent codes which were present in the source files, e.g. SLD10003, SLD10015
     */
    public Collection<String> getAgentCodes() {
        return innerMap.keySet();
    }

    public List<Pair<ProdDailyEntry, BigDecimal>> getMonth(Integer month, String agentCode) {
        return innerMap.get(agentCode).stream().filter(prodDailyEntryBigDecimalPair -> prodDailyEntryBigDecimalPair.getLeft().Month.equals(month)).collect(Collectors.toList());
    }


}
