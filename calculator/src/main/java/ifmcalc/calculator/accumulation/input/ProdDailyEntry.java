package ifmcalc.calculator.accumulation.input;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ProdDailyEntry {
    public final String AgencyCode;
    public final String AgentCode;
    public final String PolicyNo;
    public final Date AppropriationDate;
    public final Integer Month; // this is the month in the header of the PRODDAILY file itself
    public final Integer Year; // this is the year in the header of the PRODDAILY file itself
    public final BigDecimal RegularFYP;
    public final BigDecimal APE;
}
