package ifmcalc.calculator.accumulation;

import ifmcalc.calculator.accumulation.input.ProdDailyEntry;
import ifmcalc.calculator.accumulation.output.AccumulationResult;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
public class Accumulator {

    private final List<ProdDailyEntry> entries = new ArrayList<>();

    public void addProdDailySet(List<ProdDailyEntry> l) {
        entries.addAll(l);
    }

    public void clearProdDailyEntries() {
        entries.clear();
    }

    public AccumulationResult accumulate(int year) {
        List<ProdDailyEntry> _entries = entries.stream()
                .filter(prodDailyEntry -> prodDailyEntry.Year == year)
                .collect(Collectors.toList());

        _entries.sort(Comparator.comparingInt((ProdDailyEntry pe) -> pe.Month).thenComparing(pe -> pe.APE).thenComparing(pe -> pe.AppropriationDate));
        Map<String, List<Pair<ProdDailyEntry, BigDecimal>>> innerMap = new HashMap<>();

        Map<String, BigDecimal> agentCodeToRunningTotal = new HashMap<>();
        for (ProdDailyEntry e : _entries) agentCodeToRunningTotal.put(e.AgentCode, new BigDecimal(0));
        for (String agentCode : agentCodeToRunningTotal.keySet()) innerMap.put(agentCode, new ArrayList<>());

        for (ProdDailyEntry e : _entries) {
            BigDecimal newTotal = agentCodeToRunningTotal.get(e.AgentCode).add(e.APE);
            agentCodeToRunningTotal.put(e.AgentCode, newTotal);
            innerMap.get(e.AgentCode).add(new ImmutablePair<>(e, newTotal));
        }

        AccumulationResult accumulationResult = new AccumulationResult(year, innerMap);

        return accumulationResult;

    }
}
