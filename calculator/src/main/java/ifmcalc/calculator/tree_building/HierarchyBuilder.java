package ifmcalc.calculator.tree_building;

import ifmcalc.calculator.tree_building.input.AgentEntry;
import ifmcalc.calculator.tree_building.output.Advisor;
import ifmcalc.calculator.tree_building.output.HierarchyResult;
import ifmcalc.calculator.tree_building.output.Member;
import ifmcalc.calculator.tree_building.output.iBuddy;

import java.util.*;
import java.util.stream.Collectors;

public class HierarchyBuilder {
    /**
     * Builds a tree from the adjacency-map-like structure.
     *
     * @param agentCodeToMember Existing map of String to Member
     * @param agentCode         String
     */
    private void createMember(Map<String, Member> agentCodeToMember, String agentCode) {
        Member m;
        if (agentCode.startsWith(Constants.AGENT_CODE_ADVISOR_PREFIX)) {
            m = new Advisor(agentCode);
        } else {
            m = new iBuddy(agentCode);
        }
        agentCodeToMember.put(agentCode, m);
    }

    private boolean isAdvisor(String agentCode) {
        return agentCode.startsWith(Constants.AGENT_CODE_ADVISOR_PREFIX);
    }

    private boolean isiBuddy(String agentCode) {
        return agentCode.startsWith(Constants.AGENT_CODE_IBUDDY_PREFIX);
    }

    public HierarchyResult build(List<AgentEntry> agentEntryList) throws IllegalArgumentException {

        Map<String, Member> agentCodeToMember = new HashMap<>();
        Set<String> rootAdvisors = new HashSet<>(); // advisors who have a null parent
        Set<String> rootiBuddies = new HashSet<>(); // iBuddies who have an advisor for a parent.

//        Set<String> children = new HashSet<>();

        for (AgentEntry e : agentEntryList) {
            // create the parent first
            if (e.DirectAgtCode != null && !agentCodeToMember.containsKey(e.DirectAgtCode)) {
                createMember(agentCodeToMember, e.DirectAgtCode);
            }

            // then create the child
            if (!agentCodeToMember.containsKey(e.AgentCode)) {
                createMember(agentCodeToMember, e.AgentCode);
            }

            Member child = agentCodeToMember.get(e.AgentCode);

            if (e.DirectAgtCode != null) {
                // now append the child to the parent list.
                Member parent = agentCodeToMember.get(e.DirectAgtCode);

                if (isAdvisor(child.getAgentCode())
                        && isiBuddy(parent.getAgentCode())) {
                    throw new IllegalArgumentException(String.format("%s advisor cannot be the child of iBuddy %s!", child.getAgentCode(), parent.getAgentCode()));
                } else if (isiBuddy(child.getAgentCode())
                        && isAdvisor(parent.getAgentCode())) {
                    rootiBuddies.add(child.getAgentCode());
                }

                parent.addChild(child);
            }
            // directagtcode is null
            else if (isAdvisor(child.getAgentCode())) {
                rootAdvisors.add(child.getAgentCode());
            } else if (isiBuddy(child.getAgentCode())) {
                throw new IllegalArgumentException(String.format("iBuddy %s is not reporting to anyone", child.getAgentCode()));
            }

        }

        return new HierarchyResult(agentCodeToMember, rootAdvisors.stream().map(code -> (Advisor) agentCodeToMember.get(code)).collect(Collectors.toList()), rootiBuddies.stream().map(code -> (iBuddy) agentCodeToMember.get(code)).collect(Collectors.toList()));

    }
}
