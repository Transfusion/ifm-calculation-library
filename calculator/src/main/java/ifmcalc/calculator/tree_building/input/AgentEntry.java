package ifmcalc.calculator.tree_building.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class AgentEntry {

    @JsonProperty("AgentCode")
    public String AgentCode;
    @JsonProperty("DirectAgtCode")
    public String DirectAgtCode;
    @JsonProperty("LegalName")
    public String LegalName;
//    @JsonProperty("RecruitDate")
//    public Date RecruitDate;
    // more fields to come later if necessary
}
