package ifmcalc.calculator.tree_building.output;

import java.util.List;

/**
 * The tree as directly represented by the adjacency list constructed from the Agent JSON
 */

public interface Member {
    public static final String IBUDDY = "IBUDDY";
    public static final String ADVISOR = "ADVISOR";

    String getType();

    Member getParent();
    void setParent(Member member);

    String getAgentCode();

    List<Member> getChildren();
    boolean addChild(Member child);
}
