package ifmcalc.calculator.tree_building.output;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class HierarchyResult {
    public final Collection<Advisor> rootAdvisors;
    public final Collection<iBuddy> rootiBuddies;
    public final Map<String, Member> agentCodeToMember;

    public HierarchyResult(Map<String, Member> agentCodeToMember, Collection<Advisor> rootAdvisors, Collection<iBuddy> rootiBuddies) {
        this.agentCodeToMember = agentCodeToMember;

        this.rootAdvisors = rootAdvisors;
        this.rootiBuddies = rootiBuddies;
    }

    public Member getMemberByAgentCode(String agentCode) {
        return this.agentCodeToMember.get(agentCode);
    }
}
