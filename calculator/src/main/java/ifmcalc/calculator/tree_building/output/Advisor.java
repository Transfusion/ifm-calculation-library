package ifmcalc.calculator.tree_building.output;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Advisor implements Member {

    private final String agentCode;
    private final List<Member> children = new ArrayList<>();
    private Member parent = null;

    public Advisor(String agentCode) {
        this.agentCode = agentCode;
    }

    @Override
    public String getType() {
        return Member.ADVISOR;
    }

    @Override
    public Member getParent() {
        return parent;
    }

    @Override
    public void setParent(Member member) {
        this.parent = member;
    }

    @Override
    public String getAgentCode() {
        return agentCode;
    }

    @Override
    public List<Member> getChildren() {
        return children;
    }

    @Override
    public boolean addChild(Member child) {
        // check if exists
        if (children.stream().anyMatch(member -> member.getAgentCode().equals(child.getAgentCode())))
            return false;
        children.add(child);
        child.setParent(this);
        return true;
    }

    @Override
    public String toString() {
        return String.format("%s %s [%n%s%n]", agentCode, Optional.ofNullable(getParent()).map(Member::getAgentCode).orElse(null), getChildren());
    }
}
