package ifmcalc.app;

import ifmcalc.calculator.accumulation.Accumulator;
import ifmcalc.calculator.accumulation.input.ProdDailyEntry;
import ifmcalc.calculator.accumulation.output.AccumulationResult;
import ifmcalc.parser.ProdDailyParser;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccumulationTest {
    /**
     * Sanity check against manually calculated values
     */
    @Test
    void testAccumulateOctober2021() {
        assertEquals("Hello      World!", MessageUtils.getMessage());
    }

    private List<Path> getTestMonthFolders() throws IOException, URISyntaxException {
        List<Path> folders = new ArrayList<>();

        URL testInterfaceFilesPath = Objects.requireNonNull(getClass().getClassLoader().getResource("ifm-interface-files"));

        try (Stream<Path> paths = Files.walk(Paths.get(testInterfaceFilesPath.toURI()), 1)) {
            paths.forEach(path -> {
                String fileName = (path.getFileName().toString());
                if (fileName.startsWith("IFM") && !fileName.endsWith("_ORIGINAL")) {
                    folders.add(path);
                }
            });
        }
        return folders;
    }

    private Accumulator prepareAccumulator() throws IOException, ParseException, URISyntaxException {
        Logger logger = Logger.getLogger("Main");
        // read everything

        List<Path> folders = getTestMonthFolders();

        Accumulator accumulator = new Accumulator();
        ProdDailyParser prodDailyParser = new ProdDailyParser();

        // for each folder, accumulate the proddaily files first..
        for (Path monthFolder : folders) {
            Path prodDailyPath = null;
            try (Stream<Path> stream = Files.list(monthFolder)) {
                Optional<Path> g = stream.filter(path -> {
                    String fileName = (path.getFileName().toString());
                    return fileName.contains("PRODDAILY");
                }).findFirst();
                prodDailyPath = g.get();
            }

            logger.log(Level.INFO, prodDailyPath.toString());
            String prodDailyCSV = new String(Files.readAllBytes(prodDailyPath), StandardCharsets.UTF_8);

            List<ProdDailyEntry> prodDailyEntries = prodDailyParser.parseProdDailyCSV(prodDailyCSV);

            accumulator.addProdDailySet(prodDailyEntries);
        }
        return accumulator;
    }

    @Test
    void testAccumulateSeptember2021() throws IOException, URISyntaxException, ParseException {
        Accumulator accumulator = prepareAccumulator();
        AccumulationResult result = accumulator.accumulate(2021);

        /**
         * There should be only this entry
         * Month	Agency Code	Agent Code	Policy No	Appropriation Date	Single FYP	Single Top-Up	Regular FYP	Regular Top-Up	Regular Single Top-Up	APE
         *
         * 9	IFM01	SLD10015	L5932931	8/6/21	0	0	1000	0	0	6000
         *
         */
        List<Pair<ProdDailyEntry, BigDecimal>> Sept2021_SLD10015 = result.getMonth(8, "SLD10015");

        assertEquals(Sept2021_SLD10015.size(), 1);
        Pair<ProdDailyEntry, BigDecimal> september2021OnlyEntry = Sept2021_SLD10015.get(0);

        assertEquals(september2021OnlyEntry.getLeft().getAgentCode(), "SLD10015");
        assertEquals(september2021OnlyEntry.getLeft().PolicyNo, "L5932931");

        assertEquals(september2021OnlyEntry.getRight().compareTo(new BigDecimal(24000)), 0);

        List<Pair<ProdDailyEntry, BigDecimal>> Sept2021_SLD10011 = result.getMonth(8, "SLD10011");

        assertEquals(Sept2021_SLD10011.size(), 10);
        Pair<ProdDailyEntry, BigDecimal> Sept2021_SLD10011_Last = Sept2021_SLD10011.get(Sept2021_SLD10011.size() - 1);

        assertEquals(Sept2021_SLD10011_Last.getLeft().getPolicyNo(), "L5920185");
        assertEquals(Sept2021_SLD10011_Last.getLeft().getAgentCode(), "SLD10011");

        assertEquals(Sept2021_SLD10011_Last.getRight().compareTo(new BigDecimal(86640)), 0);

        assertEquals(Sept2021_SLD10011_Last.getLeft().getAPE().compareTo(new BigDecimal(12000)), 0);

    }


//    @Test
//    void testAccumulateOctober2021() throws IOException, URISyntaxException, ParseException {
//
//    }
}
