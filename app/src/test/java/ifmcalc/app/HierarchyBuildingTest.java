package ifmcalc.app;

import ifmcalc.calculator.tree_building.HierarchyBuilder;
import ifmcalc.calculator.tree_building.input.AgentEntry;
import ifmcalc.calculator.tree_building.output.HierarchyResult;
import ifmcalc.calculator.tree_building.output.Member;
import ifmcalc.parser.AgentJSONParser;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class HierarchyBuildingTest {

    private List<AgentEntry> getAgentEntries() throws IOException, URISyntaxException {
        URL testAgentTreePath = Objects.requireNonNull(getClass().getClassLoader().getResource("ifm-interface-files/Agent_Reporting_Tree/Agent_2021-11-05T09.00.04.746Z.json"));

        File f = new File(testAgentTreePath.toURI());

        String agtTblText = new String(Files.readAllBytes(f.toPath()), StandardCharsets.UTF_8);
        AgentJSONParser parser = new AgentJSONParser();
        return parser.parseAgentJSON(agtTblText);
    }

    @Test
    void testNov2021TreeBuilding() throws IOException, URISyntaxException {
        List<AgentEntry> agentEntries = getAgentEntries();

        HierarchyBuilder builder = new HierarchyBuilder();
        HierarchyResult result = builder.build(agentEntries);

        assertEquals(result.rootAdvisors.size(), 1);
        assertEquals(result.rootiBuddies.size(), 15);

        assertEquals(result.getMemberByAgentCode("I000061").getType(), Member.IBUDDY);

        assertEquals(result.getMemberByAgentCode("I000061").getAgentCode(), "I000061");

        assertNotNull(result.getMemberByAgentCode("I000061").getParent());
        assertEquals(result.getMemberByAgentCode("I000061").getParent().getAgentCode(), "SLD10011");

        assertTrue(result.getMemberByAgentCode("SLD10011").getChildren().contains(result.getMemberByAgentCode("I000061")));


        assertEquals(result.getMemberByAgentCode("SLD10004").getChildren().size(), 0);

        assertEquals(result.getMemberByAgentCode("SLD10005").getChildren().size(), 1);

        assertEquals(result.getMemberByAgentCode("SLD10001").getType(), Member.ADVISOR);
        assertNull(result.getMemberByAgentCode("SLD10001").getParent());

        assertNull(result.getMemberByAgentCode("NONEXISTENT"));
    }
}
